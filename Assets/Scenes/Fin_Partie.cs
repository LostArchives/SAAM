﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets;

public class Fin_Partie : MonoBehaviour {
    public Canvas Fin_Canvas;
    public Sprite[] poss_infos;
    private bool deja_chg = false;
    public GameObject sprite_msg;
    public Text msg_info;
    private CanvasGroup cg_msg_box;
    public GameObject joueur;
    public Canvas Score_Canvas;
    // Use this for initialization
    void Start () {
        cg_msg_box = Fin_Canvas.GetComponent<CanvasGroup>();
        joueur.GetComponent<PlayerControlLevel>().enabled = false;
        Score_Canvas.enabled = false;
        affiche("Fin de la partie \n\nTon Score : "+VariableGlobale.trans_score, "info");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnClicked(Button btn)
    {
        btn.interactable = false;
        string nom = btn.name;
        switch (nom)
        {
            

            case "btn_ok_msg_box":
                StartCoroutine(Fade(Fin_Canvas, "disparaitre"));
                Destroy((Fin_Canvas.GetComponent("GUIEffects") as MonoBehaviour));
                break;

            case "btn_retour_menu":
                StartCoroutine(change_niveau());

                break;

            default:
                break;

        }
    }

    private void info(string message, string type)
    {
        Image img = sprite_msg.GetComponent<Image>();
        switch (type)
        {
            case "erreur":
                img.sprite = poss_infos[0];
                break;
            case "info":
                img.sprite = poss_infos[1];
                break;
            case "confirmation":
                img.sprite = poss_infos[2];
                break;
        }
        msg_info.text = message;
        cg_msg_box.alpha = 0;

        GameObject obj = GameObject.Find("msg_canvas");
        if (deja_chg)
        {
            MonoBehaviour cp = obj.AddComponent<GUIEffects>() as MonoBehaviour;
        }
        deja_chg = true;
        (Fin_Canvas.GetComponent("GUIEffects") as MonoBehaviour).enabled = true;
    }

    private void affiche(string message, string type)
    {
        info(message, type);
        StartCoroutine(Fade(Fin_Canvas, "apparaitre"));
        Fin_Canvas.enabled = true;
    }

    private IEnumerator Fade(Canvas c, string mode)
    {
       // waiter = false;
        CanvasGroup cg = c.GetComponent<CanvasGroup>();

        float currentTime = 0f;
        float duree = 0.50f;
        float min = 0f;
        float max = 0f;

        switch (mode)
        {
            case "apparaitre":
                min = 0f;
                max = 1f;

                break;
            case "disparaitre":
                min = 1f;
                max = 0f;
                break;
        }
        c.enabled = true;
        while (currentTime < duree)
        {
            cg.alpha = Mathf.Lerp(min, max, currentTime / duree);
            currentTime += Time.deltaTime;
            yield return null;
        }
        if (mode == "disparaitre")
            c.enabled = false;

        yield return null;

      //  waiter = true;
    }

    private IEnumerator change_niveau()
    {
        
        GameObject bg = GameObject.Find("background");
        AudioSource s = bg.GetComponent<AudioSource>();
        s.loop = false;
        if (s.isPlaying)
            s.Stop();
        
        
        while (s.isPlaying)
        {

            yield return new WaitForSeconds(0.002f);

        }
        float fadeTime = GameObject.Find("loader_level").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
       
      

        SceneManager.LoadScene(1);
    }

}
