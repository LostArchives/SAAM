﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using Assets;
public class DestroyObjectAndAddPoint_M2 : MonoBehaviour {
    public static string trans_score;
    int myScore = 0;
    bool endLevel = false;
    //Type pair par défaut
    bool typeNumber = false;
    float beginTime = 0f;
    public float maxTimeSec;
    public float intervalTimeSec;
    public Text lbl_score;
    public Text lbl_message;
    public Text lbl_time;
    public GameObject spawner_object;
    private new AudioSource audio;
    public AudioClip[] clips;
    private float startTime;
    private float elapsedTime;
    public GameObject eventSystem;
    // Use this for initialization
    void Start()
    {
        startTime = Time.time;
        
        lbl_score.text = "Score : " + myScore;
        lbl_time.text = "Temps : " + maxTimeSec;
        lbl_message.text = "Attraper les nombres impair";
        audio = GetComponent<AudioSource>();
    }


    //Vérification de la collision
    void OnTriggerEnter2D(Collider2D collisionObjet){
     
        //On récupère la lettre de l'objet entré en collision
       
        if(collisionObjet.name.Substring(0,7).ToString().Equals("chiffre"))
        {
            char numberObject = char.ToLower(collisionObjet.name[7]);
            Debug.Log(numberObject);

            int numberCatch = int.Parse(numberObject.ToString());
            Debug.Log(numberCatch);
            if (!endLevel)
            {
                if (!typeNumber)
                {
                    if(numberObject%2 == 0)
                    {
                        myScore += 2;
                        jouer_son("Success_block");
                    }
                    else
                    {
                        myScore -= 1;
                        jouer_son("Fail_block");
                    }
                }
                else
                {
                    if (numberObject % 2 == 1)
                    {
                        myScore += 2;
                        jouer_son("Success_block");
                    }
                    else
                    {
                        myScore -= 1;
                        jouer_son("Fail_block");
                    }
                }
            }
            lbl_score.text = "Score : " + myScore;
            //On détruit l'objet
            Destroy(collisionObjet.gameObject);
        }
        
        
        

        //Si la partie n'est pas déjà finie
          
    }


	
	// Update is called once per frame
	void Update () {
        elapsedTime = Time.time - startTime;
        if (!endLevel)
            {
                //On affiche le temps
                int time = (int)(maxTimeSec - elapsedTime);
                lbl_time.text = "Temps : " + time;

                //On modifie le type de nombres à attraper !
                if (beginTime + intervalTimeSec <= elapsedTime)
                {
                    beginTime = Time.time;
                    typeNumber = !typeNumber;
                    jouer_son("switch_mode");
                }

                if (!typeNumber)
                {
                    lbl_message.text = "Attraper les nombres pair";
                }
                else
                {
                    lbl_message.text = "Attraper les nombres impair";
                }

                if (elapsedTime >= maxTimeSec)
                {
                    endLevel = true;
                    Destroy(spawner_object);
                    lbl_message.text = "Fin de la partie";
                    jouer_son("end_game");
                    StartCoroutine(Fin_Partie());

                }
            }
        
    }

    public void jouer_son(string mode)
    {
        int i = -1;
        switch(mode)
        {
            case "Success_block":
                i = 0;
                break;

            case "Fail_block":
                i = 1;
                break;

            case "switch_mode":
                i = 2;
                break;

            case "end_game":
                i = 3;
                break;
        }
        audio.clip = clips[i];
        audio.Play();
    }

    private IEnumerator Fin_Partie()
    {
        string url = "http://" +VariableGlobale.ip_serveur+ "/saam/nv_partie.php";
        WWWForm form = new WWWForm();
        form.AddField("score", myScore);
        form.AddField("id_niveau", "3");
        form.AddField("id_joueur", VariableGlobale.id_joueur);

        WWW w = new WWW(url, form);
        yield return w;
        VariableGlobale.trans_score = myScore + "";
        eventSystem.GetComponent<Fin_Partie>().enabled = true;
        yield return null;
    }
}
