﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Init_Niveau : MonoBehaviour {
    public Canvas UI_Score;
    public Canvas Start_Canvas;
    public AudioClip a_clip;
    public AudioClip a_start;
    public Text lbl_signal;
    public Button btn_start;
    public static bool wait=true;
    public GameObject joueur;
    public GameObject background;
    public Canvas Control_canvas;
	// Use this for initialization
	void Start () {
#if (UNITY_ANDROID || UNITY_IPHONE)
        {
            Control_canvas.enabled=true;
        }

#endif
        UI_Score.enabled = false;
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void signal_depart()
    {
        wait = true;
        StartCoroutine(Depart());
    }

    private IEnumerator Depart()
    {
        btn_start.interactable = false;
        AudioSource audio = GetComponent<AudioSource>();
        AudioSource audio_bg = background.GetComponent<AudioSource>();
        audio.loop = false;
        for (int i=3;i>0;i--)
        {
            lbl_signal.text = i+"";
            audio.clip = a_clip;
            audio.Play();
            yield return new WaitForSeconds(1);
        }
        lbl_signal.text = "GO";
        audio.clip = a_start;
        audio.Play();
        yield return new WaitForSeconds(0.5f);
        audio_bg.enabled = true;
        wait = false;
        joueur.GetComponent<PlayerControlLevel>().enabled = true;
        Start_Canvas.enabled = false;
        
        UI_Score.enabled = true;
        
        yield return null;
    }
}
