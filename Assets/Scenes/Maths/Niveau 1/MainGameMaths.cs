﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainGameMaths : MonoBehaviour {

    public Text lbl_calcul;
    public string chiffre1;
    public string chiffre2;
    public string chiffre3;

    public string operateur1;
    public string operateur2;

    public string resultat;

    public GameObject[] chiffres;
    public GameObject[] operateurs;
  

    // Use this for initialization
    void Start () {
        
        lbl_calcul.text += "Comment trouver "+resultat+" ?";

        StartCoroutine(Attendre_Depart());
     //   generatePrefabs();
    }

    public void generatePrefabs()
    {
        float nextPosition = -4.5F;

        //Ajout du premier nombre
        GameObject number1 = Instantiate(chiffres[int.Parse(chiffre1.ToString())]) as GameObject;
        number1.transform.position = new Vector3(nextPosition, -0.5F, transform.position.y);

        nextPosition += number1.GetComponent<BoxCollider2D>().size.x*1.5F;

        //Ajout du premier opérateur
        GameObject signe1 = getOperateur(operateur1);
        signe1.transform.position = new Vector3(nextPosition, -0.5F, transform.position.y);

        nextPosition += number1.GetComponent<BoxCollider2D>().size.x * 1.5F;

        //Ajout du second nombre
        GameObject number2 = Instantiate(chiffres[int.Parse(chiffre2.ToString())]) as GameObject;
        number2.transform.position = new Vector3(nextPosition, -0.5F, transform.position.y);

        nextPosition += number1.GetComponent<BoxCollider2D>().size.x * 1.5F;

        //Ajout du premier opérateur
        GameObject signe2 = getOperateur(operateur2);
        signe2.transform.position = new Vector3(nextPosition, -0.5F, transform.position.y);

        nextPosition += number1.GetComponent<BoxCollider2D>().size.x * 1.5F;

        //Ajout du troisième nombre
        GameObject number3 = Instantiate(chiffres[int.Parse(chiffre3.ToString())]) as GameObject;
        number3.transform.position = new Vector3(nextPosition, -0.5F, transform.position.y);

        //nextPosition += number3.GetComponent<BoxCollider2D>().size.x * 2;

    }
    
    public GameObject getOperateur(string operateur)
    {
        GameObject myObj = null;
        switch (operateur)
        {
            case "+":
                myObj = Instantiate(operateurs[0]) as GameObject;
                break;
            case "-":
                myObj = Instantiate(operateurs[1]) as GameObject;
                break;

            case "*":
                myObj = Instantiate(operateurs[2]) as GameObject;
                break;

            case "/":
                myObj = Instantiate(operateurs[3]) as GameObject;
                break;
        }

        return myObj;    
    }

    // Update is called once per frame
	void Update () {
	    
	}

    private IEnumerator Attendre_Depart()
    {
        while (Init_Niveau.wait)
        {
            yield return new WaitForSeconds(0.1f);
        }
        generatePrefabs();
        
        yield return null;
    }
}
