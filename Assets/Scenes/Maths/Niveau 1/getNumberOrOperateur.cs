﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets;
public class getNumberOrOperateur : MonoBehaviour {

    public Text lbl_message;
    public Text lbl_error;
    public int nbElements;
    public int resultat;

    string[] elementsValue;
    int count = 0;
    int myScore = 0;
    bool verifOperateur = false;
    bool verifNumber = true;
    bool endGame = false;
    private new AudioSource audio;
    public AudioClip[] a_clips;
    public GameObject eventSystem;
    // Use this for initialization
    void Start () {
        lbl_message.text = "Résultat : ";
        elementsValue = new string[nbElements];
	}

    //Vérification de la collision
    void OnTriggerEnter2D(Collider2D collisionObjet)
    {
        audio = GetComponent<AudioSource>();
        
        if (!endGame)
        {
            //On récupère la lettre de l'objet entré en collision

            Debug.Log(collisionObjet.name.Substring(0, 7).ToString());
            if (collisionObjet.name.Substring(0, 7).ToString().Equals("chiffre"))
            {
                if (verifNumber)
                {
                    char lettre = char.ToLower(collisionObjet.name[7]);
                    lbl_message.text += lettre;
                    Debug.Log(char.ToLower(collisionObjet.name[7]));
                    Destroy(collisionObjet.gameObject);
                    verifOperateur = true;
                    verifNumber = false;
                    lbl_error.text = "";
                    elementsValue[count] = lettre.ToString();
                    count++;
                    verifEndGame();

                }
                else
                {
                    lbl_error.text = "Choisissez un opérateur !";
                    jouer_son("Bad_Selec");
                }

            }
            else
            {
                Debug.Log(collisionObjet.name.ToString());
                switch (collisionObjet.name.ToString())
                {
                    case "operateurAddition(Clone)":

                        if (verifOperateur)
                        {
                            lbl_message.text += " + ";

                            verifOperateur = false;
                            verifNumber = true;
                            lbl_error.text = "";
                            elementsValue[count] = "+";
                            count++;
                            Destroy(collisionObjet.gameObject);
                            verifEndGame();
                        }
                        else
                        {
                            lbl_error.text = "Choisissez un nombre !";
                            jouer_son("Bad_Selec");
                        }

                        break;

                    case "operateurSoustraire(Clone)":

                        if (verifOperateur)
                        {
                            lbl_message.text += " - ";
                            Destroy(collisionObjet.gameObject);
                            verifOperateur = false;
                            verifNumber = true;
                            lbl_error.text = "";
                            elementsValue[count] = "-";
                            count++;
                            verifEndGame();
                        }
                        else
                        {
                            lbl_error.text = "Choisissez un nombre !";
                            jouer_son("Bad_Selec");
                        }

                        break;

                    case "operateurMultiplication(Clone)":

                        if (verifOperateur)
                        {
                            lbl_message.text += " * ";
                            Destroy(collisionObjet.gameObject);
                            verifOperateur = false;
                            verifNumber = true;
                            lbl_error.text = "";
                            elementsValue[count] = "*";
                            count++;
                            verifEndGame();
                        }
                        else
                        {
                            lbl_error.text = "Choisissez un nombre !";
                            jouer_son("Bad_Selec");
                        }

                        break;

                    case "operateurDiviser(Clone)":

                        if (verifOperateur)
                        {
                            lbl_message.text += " / ";
                            Destroy(collisionObjet.gameObject);
                            verifOperateur = false;
                            verifNumber = true;
                            lbl_error.text = "";
                            elementsValue[count] = "/";
                            count++;
                            verifEndGame();
                        }
                        else
                        {
                            lbl_error.text = "Choisissez un nombre !";
                            jouer_son("Bad_Selec");
                        }

                        break;
                }
            }
        }  
    }

    public void verifEndGame()
    {
        for (int i = 0; i < elementsValue.Length; i++)
        {
            if(elementsValue[i] != null)
            {
                Debug.Log(elementsValue[i].ToString());
            }      
        }

        if (count >= nbElements)
        {
            
            if(count == 3)
            {
                endGame = true;
                lbl_error.text = "Fin de la partie !";

                int resultatObtenu = 0;
                switch (elementsValue[1].ToString())
                {
                    case "+" :
                        resultatObtenu = int.Parse(elementsValue[0]) + int.Parse(elementsValue[2]);
                        break;

                    case "-":
                        resultatObtenu = int.Parse(elementsValue[0]) - int.Parse(elementsValue[2]);
                        break;

                    case "*":
                        Debug.Log(elementsValue[0] + " " + elementsValue[1] + " " + elementsValue[2]);

                        resultatObtenu = int.Parse(elementsValue[0]) * int.Parse(elementsValue[2]);
                        break;

                    case "/":
                        resultatObtenu = int.Parse(elementsValue[0]) / int.Parse(elementsValue[2]);
                        break;
                }
                Debug.Log(elementsValue[0] + " " + elementsValue[1] + " " + elementsValue[2]);
                lbl_message.text += " = " + resultatObtenu;
                if(resultat == resultatObtenu)
                {
                    lbl_error.text += " Vous avez gagné";
                    jouer_son("Success");
                    myScore += 10;
                }
                else
                {
                    lbl_error.color = Color.blue;
                    lbl_error.text += " Vous avez perdu";
                    jouer_son("Fail");
                    myScore -= 5;
                }
                StartCoroutine(Fin_Partie());
            }
            else
            {
                //5 elements
                if(count == 5)
                {
                    endGame = true;
                    lbl_error.text = "Fin de la partie !";

                    int resultatObtenu = 0;

                    //Premier switch
                    switch (elementsValue[1])
                    {
                        case "+":
                            resultatObtenu = int.Parse(elementsValue[0]) + int.Parse(elementsValue[2]);
                            break;

                        case "-":
                            resultatObtenu = int.Parse(elementsValue[0]) - int.Parse(elementsValue[2]);
                            break;

                        case "*":
                            resultatObtenu = int.Parse(elementsValue[0]) * int.Parse(elementsValue[2]);
                            break;

                        case "/":
                            resultatObtenu = int.Parse(elementsValue[0]) / int.Parse(elementsValue[2]);
                            break;
                    }

                    //Second switch
                    switch (elementsValue[3])
                    {
                        case "+":
                            resultatObtenu += int.Parse(elementsValue[4]);
                            break;

                        case "-":
                            resultatObtenu -= int.Parse(elementsValue[4]);
                            break;

                        case "*":
                            resultatObtenu *= int.Parse(elementsValue[4]);
                            break;

                        case "/":
                            resultatObtenu = resultatObtenu / int.Parse(elementsValue[4]);
                            break;
                    }

                    lbl_message.text += " = " + resultatObtenu;

                    if (resultat == resultatObtenu)
                    {
                        lbl_error.color = Color.blue;
                        lbl_error.text += " Vous avez gagné";
                        myScore += 10;
                        jouer_son("Success");
                    }
                    else
                    {
                        lbl_error.text += " Vous avez perdu";
                        myScore -= 5;
                        jouer_son("Fail");
                    }
                    StartCoroutine(Fin_Partie());
                }
                
                
            }
        }
    }
    // Update is called once per frame
    void Update () {
       
	}

    public void jouer_son(string mode)
    {
        int i=-1;
        switch(mode)
        {
            case "Success":
                i = 0;
                break;
            case "Fail":
                i = 1;
                break;
            case "Bad_Selec":
                i = 2;
                break;
        }
        audio.clip = a_clips[i];
        audio.Play();
    }

    private IEnumerator Fin_Partie()
    {
        string url = "http://" + VariableGlobale.ip_serveur + "/saam/nv_partie.php";
        WWWForm form = new WWWForm();
        form.AddField("score", myScore);
        form.AddField("id_niveau", "2");
        form.AddField("id_joueur", VariableGlobale.id_joueur);

        WWW w = new WWW(url, form);
        yield return w;

        VariableGlobale.trans_score = myScore+"";
        eventSystem.GetComponent<Fin_Partie>().enabled = true;
        yield return null;
    }
}
