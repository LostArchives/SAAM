﻿using UnityEngine;
using System.Collections;

public class SpawnObjects : MonoBehaviour {

    private Vector3 startPosition;
    private float newXPos = 0; 
    public float moveSpeed = 1f; //<=> de 1.0f
    public float moveDistance = 4f;

    public float timeLeftUntilSpawn = 0f;
    public float startTime = 0f;
    public float secondsBetweenSpawn = 2f;
    
    //Tableau d'objet
    public GameObject[] gameObjectSet;

    // Use this for initialization
    void Start()
    {
        //int item = Random.Range(0, gameObjectSet.Length);
        startPosition = transform.position;
        //GameObject myObj = Instantiate(gameObjectSet[item]) as GameObject;
        //myObj.transform.position = transform.position;

    }

    // Update is called once per frame
    void Update () {
        newXPos = Mathf.PingPong(Time.time * moveSpeed, moveDistance) - (moveDistance/2f);
        transform.position = new Vector3(newXPos, startPosition.y, startPosition.z);
        
        timeLeftUntilSpawn = Time.time - startTime;
        //Debug.Log(Time.time + " " + startTime);
  
        if (timeLeftUntilSpawn >= secondsBetweenSpawn)
        {
            startTime = Time.time;
            timeLeftUntilSpawn = 0;
            int item = Random.Range(0, gameObjectSet.Length);
            startPosition = transform.position;
            GameObject myObj = Instantiate(gameObjectSet[item]) as GameObject;
            myObj.transform.position = transform.position;
        }
	}
}
