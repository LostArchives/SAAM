﻿using UnityEngine;
using CnControls;

public class PlayerControlLevel : MonoBehaviour {
    private float h=1f;
    public float velocity;
    bool pr_slide = false;
    public bool jump = false;
    public bool run = true;
    public bool facingRight = true;

    public float moveForce = 365f;          // Amount of force added to move the player left and right.
    public float maxSpeed = 5f;             // The fastest the player can travel in the x axis.

    public float jumpForce;         // Amount of force added when the player jumps.
   
    private bool grounded = false;
    private bool wall = false;

    private Animator anim;
    private Transform groundCheck;
    private Transform wallCheck;

	// Use this for initialization
	void Start () {
        groundCheck = transform.Find("groundCheck");
        wallCheck = transform.Find("wallCheck");
        anim = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground")); //Verification si touche le sol
        wall = Physics2D.Linecast(transform.position, wallCheck.position, 1 << LayerMask.NameToLayer("Wall")); //Verification si touche un mur
        if (CnInputManager.GetButtonDown("Jump")&&grounded) //Si le personnage saute
            jump = true;
        if (h!=0) //Si une force est appliquée
        {
            anim.SetTrigger("Run"); //Animation de courir
            run = true;
        }
       
        else //Sinon
        {
           run = false;
        }
        if (grounded) //Si touche le sol
        {
            anim.SetTrigger("StopJumping"); //Arrêt du saut
            if (pr_slide) //Si prêt pour le slide (glisser)
            {
                if (velocity > 0.05 || velocity < -0.05) //Si c'est un saut en "cloche" et pas un saut à la verticale
                {
                        anim.SetTrigger("Slide"); //Animation de slide
                        pr_slide = false;  
                }
            }
        }
        
    }

    void FixedUpdate()
    {
     
      h= CnInputManager.GetAxis("Horizontal");       //On récupère la force exercée sur l'axe horizontal

        if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed) //Si le personnage n'a pas atteint sa vitesse maximale
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce); //On continue de le faire accélérer
            velocity = GetComponent<Rigidbody2D>().velocity.x; //
        }
         
        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed) //Si le personnage a atteint sa vitesse maximale
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y); //On le bloque à sa vitesse maximale
            velocity = GetComponent<Rigidbody2D>().velocity.x; //
        }
        
        if (h > 0 && !facingRight) //Si la force exercée est vers la droite et que le personnage est tourné vers la gauche
           Tourne();
        else if (h < 0 && facingRight) //Si la force exercée est vers la gauche et que le personnage est tourné vers la droite
            Tourne();

        if (!run) 
        {
            anim.SetTrigger("StopRunning"); //Arrêt de l'animation de courir
        }
        if (jump)
        {
            anim.CrossFade("jump",0f); //Lancement de l'animation de saut
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce)); //Transfert de la force du saut sur le personnage
            jump = false; //Le personnage ne peut pas sauter s'il saute déjà
            pr_slide = true; //Préparation du slide lorsqu'il atterrira sur le sol
        }
        
    }
        void Tourne() //Méthode qui fait tourner le personnage
        {

        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void auto_move()
    {
        if (grounded)
        {
            run = true;
            
            anim.SetTrigger("Run");
            if (transform.position.x > -1.5f && transform.position.x < 1.5f)
            {    
                    run = false;
                    jump = true;    
            }

            if (wall)
            {
                h = -h;
                Tourne();
            }
        }
        
    }
}
